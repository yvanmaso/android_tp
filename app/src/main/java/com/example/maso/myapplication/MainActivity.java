package com.example.maso.myapplication;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    //cle de savoir s'il faut ajouter ou pas
    static Boolean adding=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        //ajout de l'action de remise à zéro

        if (id == R.id.action_zero)
        {
            EditText nom=(EditText)findViewById(R.id.editText);
            EditText prenom=(EditText)findViewById(R.id.editText2);
            EditText date=(EditText)findViewById(R.id.editText3);
            EditText ville=(EditText)findViewById(R.id.editText4);
            nom.setText(" ");
            prenom.setText("");
            date.setText("");
            ville.setText("");
            return true;
        }

        //action d'ajouter un nouveau composant graphique dans le meme layout
        if(!adding) {
            if (id == R.id.action_add) {
                LinearLayout a = (LinearLayout) findViewById(R.id.layout);
                TextView phone = new TextView(this);
                phone.setText("Phone");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    phone.setId(View.generateViewId());
                }
                phone.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                EditText texte = new EditText(this);
                a.addView(phone, a.getChildCount() - 1);
                a.addView(texte, a.getChildCount() - 1);
                a.invalidate();
                adding=true;
            }
        }



        // action d'Utilisation du navigateur
        if (id == R.id.action_url) {
            String url = "http://fr.wikipedia.org/";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    //appel de la methode Validate(Question

    public  void  validate(View view)
    {
        EditText nom=(EditText)findViewById(R.id.editText);
        EditText prenom=(EditText)findViewById(R.id.editText2);
        EditText date=(EditText)findViewById(R.id.editText3);
        EditText ville=(EditText)findViewById(R.id.editText4);

        //affichage des elements des TestViews
        /*
        Toast.makeText(this, nom.getText().toString() + "\n" +

                prenom.getText().toString() + "\n" +
                date.getText().toString() + "\n" +
                ville.getText().toString(), Toast.LENGTH_SHORT).show();*/

        //
        Intent Second = new Intent(MainActivity.this, SecondActivity.class);
        Personne user = new Personne(nom.getText().toString(),prenom.getText().toString(),date.getText().toString(),ville.getText().toString());
        Second.putExtra("pers",user);
        startActivity(Second);

    }



}
