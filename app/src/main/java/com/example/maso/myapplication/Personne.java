package com.example.maso.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by maso on 22/03/16.
 */
public class Personne implements Parcelable
{
    //les attributs du SecondActivity

    private String nom;
    private String prenom;
    private String date;
    private String ville;


    public Personne(String nom, String prenom, String date, String ville) {
        this.nom = nom;
        this.prenom = prenom;
        this.date = date;
        this.ville = ville;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    protected Personne(Parcel in)
    {
        nom = in.readString();
        prenom=in.readString();
        date = in.readString();
        ville = in.readString();
    }

    public static final Creator<Personne> CREATOR = new Creator<Personne>() {
        @Override
        public Personne createFromParcel(Parcel in) {
            return new Personne(in);
        }

        @Override
        public Personne[] newArray(int size) {
            return new Personne[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(nom);
        dest.writeString(prenom);
        dest.writeString(date);
        dest.writeString(ville);
    }
}
